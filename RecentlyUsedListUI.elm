import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)
import Json.Decode exposing (Decoder, string, field, index, map2)
import Http

addRecent : a -> List a -> List a
addRecent elem list = elem :: List.filter (\x -> x /= elem) list

addBounded : Int -> a -> List a -> List a
addBounded n elem lst = List.take n (addRecent elem lst)

type alias Video = 
  {
    name : String,
    id : String
  }

-- MODEL

type alias Model = 
  { 
    searchString : String,
    currentVideo : Maybe Video,
    list : List Video
  }

type Msg = 
  SearchButtonClicked
  | ClearButtonClicked
  | SearchStringEdited String
  | PlayAgain Video
  | SearchCompleted (Result Http.Error Video)

-- UPDATE

update : Msg -> Model -> (Model, Cmd Msg)
update msg model = case msg of
  SearchButtonClicked ->
    if (not (String.isEmpty model.searchString)) then
      (model, searchCmd model.searchString)
    else
      (model, Cmd.none)
      
  ClearButtonClicked -> 
    ({ model | list = [] }, Cmd.none)

  SearchStringEdited txt -> 
    ({ model | searchString = txt }, Cmd.none)

  SearchCompleted result -> case result of
    Ok video -> chooseVideo video model
    Err _ -> (model, Cmd.none) 

  PlayAgain video -> chooseVideo video model

chooseVideo : Video -> Model -> (Model, Cmd Msg)
chooseVideo video model = 
  ({ model | 
      searchString = "",
      currentVideo = Just video,
      list = addBounded 5 video model.list
  }, Cmd.none)

-- VIEW

renderItem : Video -> Html Msg
renderItem video = li [] 
  [ a [href "#", onClick (PlayAgain video)] [text video.name] ]

renderRecentList : Model -> Html Msg
renderRecentList model = ul [] (List.map renderItem model.list)

renderVideo : String -> Html Msg
renderVideo videoId =
  div [] 
    [
      iframe [src ("https://www.youtube.com/embed/" ++ videoId),
          width 560,
          height 315,
          attribute "frameborder" "0", 
          attribute "allowfullscreen" "true"] []
    ]

view : Model -> Html Msg
view model = 
  let 
    alwaysVisibleChildren = 
      [ input [ placeholder "", Html.Attributes.value model.searchString, onInput SearchStringEdited ] [],
        button [ onClick SearchButtonClicked ] [ text "Search" ],
        button [ onClick ClearButtonClicked ] [ text "Clear list" ],
        renderRecentList model
      ]

    allChildren = case model.currentVideo of 
      Just video -> alwaysVisibleChildren ++ [renderVideo video.id]
      Nothing -> alwaysVisibleChildren
  in
    div [] allChildren

--HTTP

titleDecoder : Decoder String
titleDecoder = field "snippet" (field "title" string)
    
idDecoder : Decoder String
idDecoder = field "id" (field "videoId" string)

itemVideoDecoder : Decoder Video
itemVideoDecoder = map2 Video titleDecoder idDecoder

videoDecoder : Decoder Video
videoDecoder = field "items" (index 0 itemVideoDecoder)

searchCmd : String -> Cmd Msg
searchCmd searchString = 
  let 
    baseUrl = "https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&maxResults=1&key=AIzaSyCtzT6ogJppeBibeT4q0Fy-3cR4VcbO6UQ&q="
    request = Http.get (baseUrl ++ searchString) videoDecoder
  in
    Http.send SearchCompleted request

init : (Model, Cmd Msg)
init = ({ searchString = "", 
          currentVideo = Nothing,
          list = [] }, 
          Cmd.none)

main =
  Html.program
    {
      init = init,
      view = view,
      update = update,
      subscriptions = always Sub.none
    }